# Navidad Tracking

Este Proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 8.3.17.

## Development server

Run `ng serve` para entorno de desarrollo. Posterior navegar a `http://localhost:4200/`.

## Node js

Version de node js `10.15.0`.

## Build

Run `ng build` para compilar el proyecto para ambiente de produccion. Posteriormente cargar todo del directorio `dist/`.
